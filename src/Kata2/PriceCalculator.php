<?php

declare(strict_types=1);

namespace App\Kata2;

class PriceCalculator implements PriceCalculatorInterface
{
    public function calculate(float $price, float $discount, float $tax): float
    {
        return ($price * (1 - ($discount / 100)) ) + $tax;
    }
}
