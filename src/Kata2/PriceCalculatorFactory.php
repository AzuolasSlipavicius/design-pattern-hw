<?php
declare(strict_types=1);

namespace App\Kata2;

class PriceCalculatorFactory
{
    public function __construct(private PriceCalculatorInterface $calculator)
    {
    }

    public function calculate(float $price, float $discount, float $tax):float {
        return $this->calculator->calculate($price, $discount, $tax);
    }
}