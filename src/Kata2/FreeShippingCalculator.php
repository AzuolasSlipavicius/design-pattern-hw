<?php

declare(strict_types=1);

namespace App\Kata2;

use App\Kata3\DiscountStrategy;

class FreeShippingCalculator implements PriceCalculatorInterface
{
    public function __construct(private ?DiscountStrategy $strategy = null)
    {
    }

    public function calculate(float $price, float $discount, float $tax): float
    {
        return (1 - ($discount / 100)) * $price;
    }
}
