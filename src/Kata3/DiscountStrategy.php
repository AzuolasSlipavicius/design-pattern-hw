<?php

declare(strict_types=1);

namespace App\Kata3;

use App\Kata2\PriceCalculatorInterface;

class DiscountStrategy implements PriceCalculatorInterface
{
    public function calculate(float $price, float $discount, float $tax): float
    {
        return ($price * (1 - ($discount / 100)) );
    }
}
