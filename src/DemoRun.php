<?php

declare(strict_types=1);

namespace App;

use App\Kata1\CostInterface;
use App\Kata1\Discount;
use App\Kata1\Price;
use App\Kata1\Shipping;
use App\Kata2\FreeShippingCalculator;
use App\Kata2\PriceCalculator;
use App\Kata2\PriceCalculatorFactory;
use App\Kata2\PriceCalculatorInterface;
use App\Kata3\DiscountStrategy;
use App\Kata4\DpdShippingProvider;

class DemoRun
{
    private bool $isTuesday = false;
    private float $shipping = 8;
    private float $discount = 20;
    private CostInterface $price;


    public function __construct()
    {
        $this->price = new Price(100);
    }

    public function kata1()
    {
        return (new Shipping($this->shipping, new Discount($this->discount, $this->price)))->cost();
    }

    public function kata2(PriceCalculatorInterface $calculator)
    {
        $calculatorFactory = new PriceCalculatorFactory($calculator);

        return $calculatorFactory->calculate($this->price->cost(), $this->discount, $this->shipping);
    }

    public function kata3()
    {
        $calculator = new PriceCalculatorFactory(new PriceCalculator());

        if ($this->isTuesday()) {
            $calculator = new FreeShippingCalculator(new DiscountStrategy());
        }

        return $calculator->calculate($this->price->cost(), $this->discount, $this->shipping);
    }

    public function kata4()
    {
        // Don't know approach.
        $calculatorFactory = new PriceCalculatorFactory(new PriceCalculator());
        $this->shipping = (new DpdShippingProvider())->ourCost();

        return $calculatorFactory->calculate($this->price->cost(), $this->discount, $this->shipping);
    }

    /**
     * @return bool
     */
    public function isTuesday(): bool
    {
        return $this->isTuesday;
    }

    /**
     * @param bool $isTuesday
     */
    public function setIsTuesday(bool $isTuesday): void
    {
        $this->isTuesday = $isTuesday;
    }
}
